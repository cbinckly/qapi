package qapi

import "fmt"

type Balance struct {
    Cash            float64 `json:"cash"`
    Currency        string  `json:"currency"`
    MarketValue     float64 `json:"marketValue"`    // Market value of securities in Currency
    TotalEquity     float64 `json:"totalEquity"`    // Total Equity in Currency
}

func (b Balance) String() string {
    return fmt.Sprintf("Cash: %.2f %v", b.Cash, b.Currency)
}

type Balances struct {
    Balances []Balance `json:"perCurrencyBalances"`
    Account Account
}

func (b Balances) String() string {
    str := fmt.Sprintf("Account: %v\n", b.Account)
    for _, bal := range b.Balances {
        str += fmt.Sprintf("\t%s %f\n", bal.Currency, bal.Cash)
    }
    return str
}

