package qapi

import (
    "time"
)

type Quotes struct {
    Quotes []Quote `json:"quotes"`
}

type Quote struct {
    Symbol          string  `json:"symbol"`
    SymbolId        int     `json:"symbolId"`
    Tier            string  `json:"tier"`
    BidPrice        float64 `json:"bidPrice"`
    BidSize         float64 `json:"bidSize"`
    AskPrice        float64 `json:"askPrice"`
    AskSize         float64 `json:"askSize"`
    Price           float64 `json:"lastTradePrice"`
    Volume          int     `json:"volume"`
    OpenPrice       float64 `json:"openPrice"`
    HighPrice       float64 `json:"highPrice"`
    LowPrice        float64 `json:"lowPrice"`
    Delay           int     `json:"delay"`
    Halted          bool    `json:"halted"`
    ExpiresAt       time.Time
}

