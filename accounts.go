package qapi

import ( "fmt"
         "sync"
         "encoding/json"
         "sort"
)

type Account struct {
    // IsBilling bool `json:"isBilling"`
    // IsPrimary bool `json:"isPrimary"`
    Type            string      `json:"type"`
    Number          string      `json:"number"`
    Status          string      `json:"status"`
    ClientType      string      `json:"clientAccountType"`
    Balances        []Balance   `json:"perCurrencyBalances"`
    Positions       []Position  `json:"positions"`
    Symbols         []Symbol    `json:"symbols"`
    qapi            *Qapi
    mutex           *sync.Mutex
}

func (a Account) String() string {
    return fmt.Sprintf("Number: %v, Type: %v", a.Number, a.Type)
}

type Accounts struct {
    Accounts []Account `json:"Accounts"`
    UserId int `json:"userId"`
    qapi            *Qapi
}

func (a *Account) SymbolIds() []int {
    symbolIds := make([]int, len(a.Positions))
    for i := range a.Positions {
        symbolIds[i] = a.Positions[i].SymbolId
    }

    return symbolIds
}

func NewAccounts() (accts *Accounts, err error) {
    qapi, err := InitQapi("")
    if err != nil {
        return accts, err
    }
    accts = &Accounts{ qapi: qapi }
    err = accts.getAccounts()
    if err != nil {
        return accts, err
    }
    // resc, errc := make(chan bool), make(chan error)
    for i := range accts.Accounts {
        err = accts.Accounts[i].getBalances()
        if err != nil {
            l.Error("Failed to get balances for %s: %v", 
                        accts.Accounts[i].Number, err)
            continue
        }
        err = accts.Accounts[i].getPositions()
        if err != nil {
            l.Error("Failed to get positions for %s: %v", 
                        accts.Accounts[i].Number, err)
            return accts, err
        }
        symbolIds := accts.Accounts[i].SymbolIds()
        err = accts.Accounts[i].getSymbols(symbolIds)
        if err != nil {
            l.Error("Failed to get symbols for %s: %v", 
                        accts.Accounts[i].Number, err)
            return accts, err
        }
        err = accts.Accounts[i].getQuotes()
        if err != nil {
            l.Error("Failed to get quotes for %s: %v", 
                        accts.Accounts[i].Number, err)
            return accts, err
        }
    }

    return accts, nil
}

func (a Accounts) String() string {
    str := fmt.Sprintf("User: %v, Accounts: %v\n", a.UserId, len(a.Accounts))
    for i, acct := range a.Accounts {
        str += fmt.Sprintf("\t%d: %s\n", i, acct)
    }
    return str
}

func (accts *Accounts) getAccounts() error {
    acctRespBytes, err := accts.qapi.Get("accounts")
    if err != nil {
        return err
    }
    err = json.Unmarshal(acctRespBytes, &accts)
    if err != nil {
        l.Error("Error unmarshaling Accounts request: %v", err)
        return err
    }
    for i := range accts.Accounts {
        accts.Accounts[i].qapi = accts.qapi
    }

    return nil
}

func (acct *Account) getBalances() error {
    balRespBytes, err := acct.qapi.Get("accounts/" + acct.Number + "/balances")
    if err != nil {
        return err
    }
    
    err = json.Unmarshal(balRespBytes, &acct)
    if err != nil {
        l.Error("Error unmarshaling Balances request: %v", err)
        return err
    }
    return nil
}

func (acct *Account) getPositions() error {
    positionRespBytes, err := acct.qapi.Get("accounts/" + acct.Number + "/positions")
    if err != nil {
        return err
    }
    err = json.Unmarshal(positionRespBytes, acct)
    if err != nil {
        l.Error("Error unmarshaling Positions request: %v", err)
        return err
    }
    j:= 0
    for i, pos := range acct.Positions {
        if pos.MarketValue > 0 {
            acct.Positions[j] = acct.Positions[i]
            j++
        }
    }
    acct.Positions = acct.Positions[:j]

    sort.Slice(acct.Positions, func (i, j int) bool {
        return acct.Positions[i].Symbol < acct.Positions[j].Symbol
    })

    return nil
}

func (acct *Account) getSymbols(symbolIds []int) (error) {
    symbols, err := acct.qapi.GetSymbols(symbolIds)
    if err != nil {
        return err
    }
    acct.Symbols = *symbols
    return nil
    /*
    resc, errc := make(chan Symbol), make(chan error)
    for _, symbolId := range symbolIds {
        go func(symId int) {
            symbol, err := acct.qapi.GetSymbol(symId)
            if err != nil {
                errc<- err
                return
            }
            resc<- symbol
        }(symbolId)
    }

    errs := make([]error, 0)
    syms := make([]Symbol, 0)
    for range symbolIds {
        select{
        case res := <-resc:
            syms = append(syms, res)
        case e := <-errc:
            l.Error("Error fetching symbol: %v", e)
            errs = append(errs, e)
        }
    }

    if len(errs) > 0 { return errs[0] }

    sort.Slice(syms, func (i, j int) bool {
        return syms[i].Symbol < syms[j].Symbol
    })

    acct.Symbols = syms
   
    return nil
    */
}

func (acct *Account) getQuotes() (error) {
    err := acct.qapi.GetQuotes(acct.Symbols)
    if err != nil {
        l.Error("Failed to get quotes: %v", err)
        return err
    }
    return nil
}

