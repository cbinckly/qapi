package qapi

import ( "fmt"
         "encoding/json"
         "net/http"
         "bytes"
         "io/ioutil"
         "log"
         "time"
         "errors"
         "sync"
         "bitbucket.org/cbinckly/utes"
)

const CONFIG string = "/etc/frija/fconfig.go.json"
var l = &utes.Logger{ Package: "qapi.qapi", Level: utes.DEBUG }

var qapi_instance = &Qapi{}
var once sync.Once

/* Qapi Type
*
*  A type that interfaces with the Questrade API.
*/
type Api interface {
    Close() bool
    Refresh() (Qauth, error)
    IsAuthenticated() bool
    TokenExpired() bool
    SetToken(token string) (Qauth, error)
    Get(path string) ([]byte, error)
}

type Qapi struct {
    Auth Qauth `json:"qapi"`
    symbolCache map[int]Symbol
    quoteCache map[int]Quote
} 

type QapiResp struct {
    Code        int     `json:"code"`
    Message     string  `json:"message"`
}

func InitQapi(token string) (*Qapi, error) {

    if qapi_instance.Auth.Token == "" {
        l.Debug("Creating a new Qapi instance.")
        qapi_instance = &Qapi{ 
                            symbolCache: make(map[int]Symbol),
                            quoteCache: make(map[int]Quote) }

        if !qapi_instance.readFromDisk() {
            if token == "" {
                return qapi_instance, errors.New(fmt.Sprintf("failed to load config at %s and no token provided", CONFIG))
            }
        }

        if token != "" && qapi_instance.Auth.Token == "" {
            qapi_instance.SetToken(token)
        }
    }

    return qapi_instance , nil
}

func (q *Qapi) Close() bool {
    if q.writeToDisk() {
        log.Println("Wrote config to", CONFIG)
        return true
    }
    return false
}

type Qauth struct {
    Token string `json:"access_token"`
    RefreshToken string `json:"refresh_token"`
    ApiServer string `json:"api_server"`
    ExpiresAt time.Time
    ExpiresIn float32 `json:"expires_in"`
    TokenType string `json:"token_type"`
    Authenticated bool
}

func (q *Qapi) String() string {
    return fmt.Sprintf("%v", q.Auth)
}

func (q *Qapi) IsAuthenticated() bool {
    if !q.Auth.Authenticated || q.TokenExpired() {
        q.Refresh()
    }

    return q.Auth.Authenticated
}

func (q *Qapi) Refresh() (Qauth, error) {
    readBuf := []byte("")

    if q.Auth.RefreshToken == "" {
        return q.Auth, errors.New("No refresh token, reset from website.")
    }
    q.Auth.Authenticated = false

    url := fmt.Sprintf("https://login.questrade.com/oauth2/token?grant_type=refresh_token&refresh_token=%s", q.Auth.RefreshToken)

    req, err := http.NewRequest("GET", url, bytes.NewBuffer(readBuf))

    client := &http.Client{}
    resp, err := client.Do(req)

    if err != nil {
        l.Error("Failed HTTP request %#v -- %#v: %#v", req, resp, err)
        return q.Auth, err
    }
    defer resp.Body.Close()

    body, _ := ioutil.ReadAll(resp.Body)

    q.parseAuth(body)

    q.Auth.ExpiresAt = time.Now().Add(time.Duration(q.Auth.ExpiresIn) * time.Second)
    q.Auth.Authenticated = true

    q.writeToDisk()

    return q.Auth, nil
}

func (q *Qapi) writeToDisk() bool {
    obj, err := json.Marshal(q)
    if err != nil {
        return false
    }
    err = ioutil.WriteFile(CONFIG, obj, 0644)
    if err != nil {
        return false
    }
    return true
}

func (q *Qapi) readFromDisk() bool {
    obj, err := ioutil.ReadFile(CONFIG)
    if err != nil {
        l.Error("Failed to read %s: %v.", CONFIG, err)
        return false
    }
    err = json.Unmarshal(obj, &q)
    if err != nil {
        l.Error("Failed to unmarshal content %v", string(obj))
        return false
    }
    return true
}

func (q *Qapi) parseAuth(content []byte) error {
    err := json.Unmarshal(content, &q.Auth)
    if err != nil {
        l.Error("Error unmarshaling auth response [%s], %v", string(content)[:100], err)
        return err
    }
    return nil
}

func (q *Qapi) SetToken(token string) (Qauth, error) {
    q.Auth.RefreshToken = token
    q.Auth.ExpiresAt = time.Now()
    return q.Refresh()
}

func (q *Qapi) TokenExpired() bool {
    if q.Auth.ExpiresAt.After(time.Now()) {
        return false
    }
    return true
}

func (q *Qapi) Get(path string) (body []byte, err error) {

    if !q.IsAuthenticated() {
        return nil, errors.New("Not authenticated.")
    }

    readBuf := []byte("")
    url := q.Auth.ApiServer + "v1/" + path
    success := false

    // Retry twice, refreshing token if required.
    for try := 0; try < 2; try++ {
        req, err := http.NewRequest("GET", url, bytes.NewBuffer(readBuf))
        req.Header.Set("Authorization", 
                       fmt.Sprintf("%s %s", q.Auth.TokenType, q.Auth.Token))
        req.Header.Set("Content-Type", "application/json")

        client := &http.Client{}
        resp, err := client.Do(req)
        if err != nil {
            l.Error("Failed HTTP request %#v -- %#v: %#v", req, resp, err)
            return body, err
        }
        defer resp.Body.Close()

        body, _ = ioutil.ReadAll(resp.Body)

        respBuff := QapiResp{}
        err = json.Unmarshal(body, &respBuff)
        if err != nil {
            l.Error("Error unmarshaling Api request: ", err)
            return body, err
        }

        if respBuff.Code > 0 || respBuff.Message != "" {
            // Handle access token invalid
            if respBuff.Code == 1017 {
                l.Warning("Access token invalid, refreshing.")
                q.Refresh()
            } else {
                l.Error("Error in API request: code(%d), message(%s)", 
                        respBuff.Code, respBuff.Message)
                return body, errors.New(fmt.Sprintf("qapi error %d", respBuff.Code))
            }
        }

        success = true
        break
    }

    if success {
        return body, nil
    }

    return body, errors.New("api get request failed.")

}

func (q *Qapi) GetSymbol(symbolId int) (Symbol, error){

    symbol, ok := q.symbolCache[symbolId]

    if !ok {
        symbuff := Symbols{}
        symbolRespBytes, err := q.Get(fmt.Sprintf("symbols/%d", symbolId))
        if err != nil {
            l.Error("Error getting symbol", err)
            return symbol, err
        }
        err = json.Unmarshal(symbolRespBytes, &symbuff)
        if err != nil {
            l.Error("Error unmarshaling Symbols request: ", err)
            return symbol, err
        }
        q.symbolCache[symbolId] = symbuff.Symbols[0]
        symbol = symbuff.Symbols[0]
        l.Info("Not in cache: %d - %s", symbolId, symbol.Symbol)
    }
    

    return symbol, nil
}

func (q *Qapi) GetSymbols(symbolIds []int) (*[]Symbol, error) {
    failedSymbolIds := make([]int, 0)
    symbols := make([]Symbol, 0)

    for _, id := range symbolIds {
        symbol, ok := q.symbolCache[id]
        if !ok {
            failedSymbolIds = append(failedSymbolIds, id)
        } else {
            symbols = append(symbols, symbol)
        }
    }

    failed := len(failedSymbolIds)

    if failed > 0 {
        ids := ""
        for i, id := range failedSymbolIds {
            if i == failed - 1 {
                ids += fmt.Sprintf("%d", id)
            } else { 
                ids += fmt.Sprintf("%d,", id)
            }
        }
        symbolbuff := Symbols{}
        url := fmt.Sprintf("symbols?ids=%s", ids)
        symbolRespBytes, err := q.Get(url)
        if err != nil {
            l.Error("Error getting symbols", err)
            return &symbols, err
        }
        err = json.Unmarshal(symbolRespBytes, &symbolbuff)
        if err != nil {
            l.Error("Error unmarshaling Symbols request: ", err)
            return &symbols, err
        }

        for i := range symbolbuff.Symbols {
            symbols = append(symbols, symbolbuff.Symbols[i])
            q.symbolCache[symbolbuff.Symbols[i].SymbolId] = symbolbuff.Symbols[i]
        }
    } 
    return &symbols, nil
}

func (q *Qapi) GetQuote(symbols []Symbol) error {

    for i := range symbols {
        quote, ok := q.quoteCache[symbols[i].SymbolId]
        if !ok {
            quotebuff := Quotes{}
            quoteRespBytes, err := q.Get(fmt.Sprintf("markets/quotes/%d", symbols[i].SymbolId))
            if err != nil {
                l.Error("Error getting quote", err)
                return err
            }
            err = json.Unmarshal(quoteRespBytes, &quotebuff)
            if err != nil {
                l.Error("Error unmarshaling Quotes request: ", err)
                return err
            }
            q.quoteCache[symbols[i].SymbolId] = quotebuff.Quotes[0]
            symbols[i].Quote = quotebuff.Quotes[0]
            l.Info("Not in cache: %d - %.2f", symbols[i].SymbolId, symbols[i].Quote.Price)
        } else {
            symbols[i].Quote = quote
        }
    }

    return nil
}

func (q *Qapi) GetQuotes(symbols []Symbol) error {

    failedSymbols := make([]Symbol, 0)

    for i := range symbols {
        quote, ok := q.quoteCache[symbols[i].SymbolId]
        if !ok {
            failedSymbols = append(failedSymbols, symbols[i])
        } else {
            symbols[i].Quote = quote
        }
    }

    failed := len(failedSymbols)

    if failed > 0 {
        failedSymbolIds := ""
        for i, symbol := range failedSymbols {
            if i == failed - 1 {
                failedSymbolIds += fmt.Sprintf("%d", symbol.SymbolId)
            } else { 
                failedSymbolIds += fmt.Sprintf("%d,", symbol.SymbolId)
            }
        }
        quotebuff := Quotes{}
        url := fmt.Sprintf("markets/quotes?ids=%s", failedSymbolIds)
        quoteRespBytes, err := q.Get(url)
        if err != nil {
            l.Error("Error getting quote", err)
            return err
        }
        err = json.Unmarshal(quoteRespBytes, &quotebuff)
        if err != nil {
            l.Error("Error unmarshaling Quotes request: ", err)
            return err
        }

        for i := range quotebuff.Quotes {
            for j := range symbols {
                if quotebuff.Quotes[i].SymbolId == symbols[j].SymbolId {
                    symbols[j].Quote = quotebuff.Quotes[i]
                    break
                }
            }
            q.quoteCache[quotebuff.Quotes[i].SymbolId] = quotebuff.Quotes[i]
        }
    } 

    return nil
}
