package qapi

import (
    "time"
)

type Symbols struct {
    Symbols []Symbol
}

type Symbol struct {
    Symbol string `json:"symbol"`
    SymbolId int `json:"symbolId"`
    PreviousClose float64 `json:"prevDayClosePrice"`
    SecurityType string `json:"securityType"`
    Tradable bool `json:"isTradable"`
    Quotable bool `json:"isQuotable"`
    Currency string `json:"currency"`
    Dividend float64 `json:"dividend"`
    Yield float64 `json:"yield"`
    ExDividend time.Time `json:"exDiv"`
    Quote Quote
}

