package qapi

import "testing"

var acctResp = `{  "accounts": [{"clientAccountType": "Individual",
               "isBilling": true,
               "isPrimary": true,
               "number": "26448049",
               "status": "Active",
               "type": "Margin"},
              {"clientAccountType": "Corporation",
               "isBilling": false,
               "isPrimary": false,
               "number": "27160663",
               "status": "Active",
               "type": "Margin"},
              {"clientAccountType": "Individual",
               "isBilling": false,
               "isPrimary": false,
               "number": "51566265",
               "status": "Active",
               "type": "RRSP"}],
             "userId": 75193}`

 var balResp = `{"combinedBalances": [{"buyingPower": 50708.307396,
               "cash": 3857.546625,
               "currency": "CAD",
               "isRealTime": true,
               "maintenanceExcess": 15227.719939,
               "marketValue": 16642.05685,
               "totalEquity": 20499.603475},
              {"buyingPower": 37670.535173,
               "cash": 2865.720693,
               "currency": "USD",
               "isRealTime": true,
               "maintenanceExcess": 11312.473025,
               "marketValue": 12363.165329,
               "totalEquity": 15228.886023}],
        "perCurrencyBalances": [{"buyingPower": 15758.19936,
              "cash": 2682.652,
              "currency": "CAD",
              "isRealTime": true,
              "maintenanceExcess": 4732.192,
              "marketValue": 2967.7,
              "totalEquity": 5650.352},
             {"buyingPower": 25963.975957,
              "cash": 872.813777,
              "currency": "USD",
              "isRealTime": true,
              "maintenanceExcess": 7796.989777,
              "marketValue": 10158.5,
              "totalEquity": 11031.313777}],
        "sodCombinedBalances": [{"buyingPower": 50708.307396,
              "cash": 3857.546625,
              "currency": "CAD",
              "isRealTime": true,
              "maintenanceExcess": 15227.719939,
              "marketValue": 16580.15685,
              "totalEquity": 20437.703475},
             {"buyingPower": 37670.535173,
              "cash": 2865.720693,
              "currency": "USD",
              "isRealTime": true,
              "maintenanceExcess": 11312.473025,
              "marketValue": 12317.180633,
              "totalEquity": 15182.901326}],
        "sodPerCurrencyBalances": [{"buyingPower": 15758.19936,
             "cash": 2682.652,
             "currency": "CAD",
             "isRealTime": true,
             "maintenanceExcess": 4732.192,
             "marketValue": 2905.8,
             "totalEquity": 5588.452},
            {"buyingPower": 25963.975957,
             "cash": 872.813777,
             "currency": "USD",
             "isRealTime": true,
             "maintenanceExcess": 7796.989777,
             "marketValue": 10158.5,
             "totalEquity": 11031.313777}]}`
