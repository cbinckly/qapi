package qapi

type Positions struct {
    Positions []Position
    Account Account
}

type Position struct {
    Symbol          string  `json:"symbol"`
    SymbolId        int     `json:"symbolId"`
    Open            int     `json:"openQuantity"`
    OpenPnL         float64 `json:"openPnl"`
    Closed          int     `json:"closedQuantity"`
    ClosedPnL       float64 `json:"closedPnl"`
    DayPnL          float64 `json:"dayPnl"`
    MarketValue     float64 `json:"currentMarketValue"`
    Price           float64 `json:"price"`
    AveragePrice    float64 `json:"averageEntryPrice"`
    Cost            float64 `json:"totalCost"`
    RealTime        bool    `json:"realTime"`
}

func (p *Position) Return() float64 {
    return (p.OpenPnL / p.Cost) * 100
}

